package modelo;

public class Proveedor {
    int id;
    String dni;
    String nom;
    String cat;
    String dir;
    String cel;

    public Proveedor() {
    }

    public Proveedor(int id,String dni ,String nom, String cat, String dir, String cel) {
        this.id = id;
        this.dni = dni;
        this.nom = nom;
        this.cat = cat;
        this.dir = dir;
        this.cel = cel;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   public String getDni(){
       return dni;
   }
   
   public void setDni(String dni){
       this.dni = dni;
   }
           

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public String getCat(){
        return cat;
    }
    public void setCat(String cat){
        this.cat = cat;
    }
    
    public String getDir(){
        return dir;
    }
    
    public void setDir(String dir){
        this.dir = dir;
    }
    
    }

    

    

