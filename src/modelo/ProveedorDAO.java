
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ProveedorDAO {
    PreparedStatement ps;
    ResultSet rs;    
    
    Conexion acceso = new Conexion();
    Connection con;
    
    public Proveedor ValidarVendedor(String dni,String cat){
        Proveedor ev=new Proveedor();
        String sql="select * from proveedores where Dni=? and categoria_producto=?";
        try {
           con=acceso.Conectar();
           ps=con.prepareStatement(sql);
           ps.setString(1, dni);
           ps.setString(2, cat);
           rs=ps.executeQuery();
            while (rs.next()) {
                ev.setId(rs.getInt(1));
                ev.setDni(rs.getString(2));
                ev.setNom(rs.getString(3));
                ev.setCat(rs.getString(4));
                ev.setDir(rs.getString(5));
                ev.setCel(rs.getString(6));
                //ev.setUser(rs.getString(6));
            }
        } catch (Exception e) {
        }
        return ev;
    }
     public Proveedor listarProveedorId(String dni) {
        Proveedor v=new Proveedor();
        String sql = "select * from proveedores where Dni=" + dni;
        try {
            con = acceso.Conectar();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                v.setId(rs.getInt(1));
                v.setDni(rs.getString(2));
                v.setNom(rs.getString(3));
                v.setCat(rs.getString(4));
                v.setDir(rs.getString(5));
                v.setCel(rs.getString(6));
                //v.setEstado(rs.getString(5));
                //v.setUser(rs.getString(6));
            }
        } catch (Exception e) {
        }
        return v;
    }
    //********CRUD - Principal**************

    public List listarProveedor() {
        String sql = "select * from proveedores";
        List<Proveedor> listaProveedor = new ArrayList<>();
        try {
            con = acceso.Conectar();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Proveedor ven = new Proveedor();
                ven.setId(rs.getInt(1));
                ven.setDni(rs.getString(2));
                ven.setNom(rs.getString(3));
                ven.setCat(rs.getString(4));
                ven.setDir(rs.getString(5));
                ven.setCel(rs.getString(6));
                listaProveedor.add(ven);
            }
        } catch (Exception e) {
        }
        return listaProveedor;
    }

    public int agregar(Proveedor v) {
        int r=0;
        String sql = "insert into proveedores(Dni,nombre,categoria_producto,direccion,celular) values(?,?,?,?,?)";
        try {
            con = acceso.Conectar();
            ps = con.prepareStatement(sql);
            ps.setString(1, v.getDni());
            ps.setString(2, v.getNom());
            ps.setString(3, v.getCat());
            ps.setString(4, v.getDir());
            ps.setString(5, v.getCel());
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public int actualizar(Proveedor v) {
        int r=0;
        String sql = "update proveedores set Dni=?, nombre=?,categoria_producto=?,direccion=?, celular=? Where idproveedor=?";
        try {
            con = acceso.Conectar();
            ps = con.prepareStatement(sql);
            ps.setString(1, v.getDni());
            ps.setString(2, v.getNom());
            ps.setString(3, v.getCat());
            ps.setString(4, v.getDir());
            ps.setString(5, v.getCel());
            ps.setInt(6, v.getId());
            r = ps.executeUpdate();
            if (r == 1) {
                r = 1;
            } else {
                r = 0;
            }
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return r;
    }

    public int delete(int id) {
        int r=0;
        String sql = "delete from proveedores where idproveedores=?";
        try {
            con = acceso.Conectar();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }
}
