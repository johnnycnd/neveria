package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CompraDAO {

    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    int r=0;
         
    public int idProducto(String nombre){
        int id = 0;
        String sql="select idProducto from producto where Nombres = ?";
        try {
            con=cn.Conectar();
            ps=con.prepareStatement(sql);
            ps.setString(1, nombre);
            rs=ps.executeQuery();
            while (rs.next()) {
                id=rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return id;
    }
      public String nombreProveedor(int id) {
          String nombre = "";
        String sql = "select nombre from proveedores where Dni = ?";
        List<Proveedor> listaProveedor = new ArrayList<>();
        try {
            con = cn.Conectar();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Proveedor ven = new Proveedor();
               nombre = rs.getString(1);
                listaProveedor.add(ven);
            }
        } catch (Exception e) {
        }
        return nombre;
    }
    public int GuardarCompras(Compra c){       
        String sql="insert into compra(IdCompra,cantidad,fkproveedor,tipo_producto,costo,especificacion,fecha_compra)values(?,?,?,?,?,?,?)";
        try {
            con=cn.Conectar();
            ps=con.prepareStatement(sql);
            ps.setInt(1, c.getId());
            ps.setInt(2, c.getStock());
            ps.setInt(3, c.getFkproveedor());
            ps.setString(4, c.getTipo());
            ps.setDouble(5, c.getPrecio());
            ps.setString(6, c.getNom());
            ps.setString(7, c.getFecha());
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
        
        return r;
    }

}