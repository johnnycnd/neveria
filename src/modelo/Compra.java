
package modelo;

public class Compra {
    int id;
    String nom;
    double precio;
    int stock;    
    private String fecha;
    private int fkproveedor;
    private String tipo;
    public Compra() {
    }

    public Compra(int id, String nom,String tipo, double precio, int stock, String estado, String fecha,int fkproveedor) {
        this.id = id;
        this.nom = nom;
        this.precio = precio;
        this.stock = stock;        
        this.fecha = fecha;
        this.fkproveedor = fkproveedor;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

  
   
    /**
     * @return the fkproveedor
     */
    public int getFkproveedor() {
        return fkproveedor;
    }

    /**
     * @param fkproveedor the fkproveedor to set
     */
    public void setFkproveedor(int fkproveedor) {
        this.fkproveedor = fkproveedor;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

   
    
}
